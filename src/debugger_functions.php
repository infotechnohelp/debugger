<?php

function debugger__exit()
{
    if (php_sapi_name() === 'cli') {
        echo DEBUGGER__LABEL_COLOR . " EXIT " . DEBUGGER__COLOR_END . PHP_EOL;
    }

    exit;
}

function debugger__prepareArgumentString(array $args = [])
{
    array_walk($args, function (&$value) {
        if (is_string($value)) {
            $value = "\"$value\"";
        }

        if (is_array($value)) {
            $value = json_encode($value);
        }
    });

    return implode(", ", $args);
}

function debugger__prepareLabelOptionArray(string $label, bool $showLabel = true, ?string $anotherLabelToShow = null)
{
    $label = $showLabel ? $label : $anotherLabelToShow;

    return $label === null ? [] : [
        'label' => $label,
    ];
}

function debugger__loadStylesIfRequired()
{
    if(!function_exists('removeExtraSpaces')){
        function removeExtraSpaces(string $string){
            return preg_replace(
                '/\s+/',
                ' ',
                str_replace(
                    ["\t", "\r", PHP_EOL],
                    '',
                    $string
                )
            );
        }
    }

    if ($GLOBALS['debugger']['initiated'] === false) {
        $style = removeExtraSpaces(file_get_contents(dirname(__DIR__) . "/assets/style.css"));
        echo "<style>$style</style>";

        $styleExtensionPath = dirname(__DIR__, 4) . "/debugger__style.css";

        if (file_exists($styleExtensionPath)) {
            $style = removeExtraSpaces(file_get_contents($styleExtensionPath));
            echo "<style>$style</style>";
        }

        $GLOBALS['debugger']['initiated'] = true;
    }
}

function debugger__printArgs(array $args, string $outputFormat = null, callable $callback = null, array $options = [])
{
    if (php_sapi_name() !== 'cli') {
        debugger__loadStylesIfRequired();
        echo "<div class='_debugger__wrapper'><pre class='_debugger__pre'>";
    }

    $varTypeColor = DEBUGGER__VAR_TYPE_COLOR;
    $outputNumColor = DEBUGGER__OUTPUT_NUM_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;

    if (isset($options['label'])) {
        echo DEBUGGER__LABEL_COLOR . " " . $options['label'] . " $colorEnd" . PHP_EOL;
    }

    $quotedStrings = $options['quoted_strings'] ?? false;

    $vars = [
        'i' => 1,
        'arg' => null,
        'TYPE' => null,
        'type' => null, // typeString
        'output' => null,
    ];

    if (isset($options['caller'])) {
        echo $options['caller'] . PHP_EOL;
    }

    foreach ($args as $arg) {

        $type = $arg === null ? 'NULL' : null;

        $type = is_array($arg) ? 'array' : $type;

        $type = is_object($arg) ? 'object' : $type;


        $vars['arg'] = $callback($arg) ?? $arg;

        if (strpos($outputFormat, '$type') === false) {
            $vars['arg'] = $vars['arg'] === null ? 'NULL' : $vars['arg'];
        }

        $vars['TYPE'] = $type ?? gettype($vars['arg']);

        $vars['type'] = $varTypeColor . $vars['TYPE'] . $colorEnd;


        if ($vars['TYPE'] === 'string' && $quotedStrings) {
            $vars['arg'] = '"' . $vars['arg'] . '"';
        }


        $vars['output'] = $outputFormat;


        foreach (['arg', 'type', 'i'] as $var) {

            if ($var === 'i') {
                $vars['output'] = str_replace(
                    '$' . $var,
                    $outputNumColor . " {$vars['i']} $colorEnd",
                    $vars['output']
                );
            } else {
                $vars['output'] = str_replace('$' . $var, $vars[$var], $vars['output']);
            }

        }

        if (strpos($outputFormat, '$type') !== false && $vars['TYPE'] === 'array') {
            $vars['output'] = str_replace("({$varTypeColor}array$colorEnd)", '', $vars['output']);
        }

        echo $vars['output'];

        $vars['i']++;
    }

    if (php_sapi_name() !== 'cli') {
        echo "</pre></div>";
    }
}

function debugger__mergeCliConfigs(array $cliConfig, array $cliConfigExtension)
{
    $cliConfig = array_merge_recursive($cliConfig, $cliConfigExtension);

    array_walk($cliConfig['color'], function(&$value){
        if(is_array($value)){
            $value = array_reverse($value)[0];
        }
    });

    return $cliConfig;
}