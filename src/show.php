<?php

function debugger__show(array $args, bool $showLabel = true, ?string $anotherLabelToShow = null)
{
    $label = $showLabel ? 'SHOW' : $anotherLabelToShow;

    $options = $label === null ? [] : [
        'label' => $label,
    ];

    debugger__printArgs(
        $args,
        "\$i \$arg" . PHP_EOL,
        function ($arg) {
            if (is_array($arg)) {
                return json_encode($arg, JSON_PRETTY_PRINT);
            }

            if (is_object($arg)) {
                ob_start();
                print_r($arg);
                $print_r = ob_get_clean();
                return $print_r;
            }
        },
        debugger__prepareLabelOptionArray('SHOW', $showLabel, $anotherLabelToShow)
    );
}

function show()
{
    call_user_func_array('debugger__show', [func_get_args()]);
}

function showexit()
{
    call_user_func_array('show', func_get_args());
    debugger__exit();
}