<?php

function debugger__trace(bool $removeFirstBacktraceItem = true)
{
    $pointerColor = DEBUGGER__POINTER_COLOR;
    $functionTitleColor = DEBUGGER__FUNCTION_TITLE_COLOR;
    $functionArgsColor = DEBUGGER__FUNCTION_ARGS_COLOR;
    $dirPathColor = DEBUGGER__DIR_PATH_COLOR;
    $colorEnd = DEBUGGER__COLOR_END;


    $debug_backtrace = debug_backtrace();

    if ($removeFirstBacktraceItem) {
        $debug_backtrace = array_slice($debug_backtrace, 1);
    }

    foreach (array_reverse($debug_backtrace) as $item) {
        $argString = $functionArgsColor . debugger__prepareArgumentString($item['args']) . $colorEnd;

        $fileTitle = basename($item['file']);
        $dirPath = dirname($item['file']);

        $backtrace[] = "$pointerColor $fileTitle:{$item['line']} $colorEnd $functionTitleColor{$item['function']}$colorEnd($argString) $dirPathColor$dirPath$colorEnd";
    }

    call_user_func_array('debugger__show', [$backtrace, false, 'TRACE']);
}


function trace()
{
    call_user_func('debugger__trace');
}

function traceexit()
{
    call_user_func('debugger__trace');
    debugger__exit();
}