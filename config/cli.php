<?php return [
    "color" => [
        "label" => "\e[1;32;40m", // bold green, bg black
        "pointer" => "\e[1;38;40m", // bold white, bg black
        "end" => "\e[0m",
        "functionTitle" => "\e[1;32;1m", // bold green, no bg
        "functionArgs" => "\e[4;38;1m", // underlined white, no bg
        "dirPath" => "\e[0;37;1m", // grey, no bg
        "varType" =>  "\e[1;32;1m", // bold green, no background
        "outputNum" => "\e[1;30;47m", // bold black, grey background
    ]
];